package inventory.validators;

public interface Validator<T, Exception extends RuntimeException> {
    void isValid(T object) throws Exception;
}
