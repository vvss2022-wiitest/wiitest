package inventory.service;


import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InventoryService {

    private InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }

    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min,
                max, partDynamicValue);
        try {
            repo.addPart(inhousePart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock,
                min, max, partDynamicValue);
        try {
            repo.addPart(outsourcedPart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {

        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock,
                                  int min, int max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock,
                                     int min, int max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min,
                              int max, ObservableList<Part> addParts){
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }


    /**
     * Get a specific product that has more parts where
     * the price is bigger than minimumPrice than minimumNrParts and return a new
     * product with just the parts that have the price > minimumPrice
     * if the product doesn't have any parts that meet the condition throw an error
     * if the product with the name is not found throw an error
     *
     * @param name : name of the component
     * @param minimumPrice : the price that a part should have to be returned, value > 0
     */
    public Product getSpecialProduct(String name, Integer minimumPrice) throws Exception {
        if(minimumPrice <= 0){
            throw new Exception("Date de intrare invalide");
        }
        Product product = repo.lookupProduct(name);
        if(product == null){
            throw new Exception("Produsul nu a fost gasit");
        } else {
            Product newProduct = new Product(product.getProductId(),
                    product.getName(), product.getPrice(), product.getInStock(), product.getMin(),
                    product.getMax(), FXCollections.observableArrayList());
            for (Part part: product.getAssociatedParts()) {
                if(part.getPrice() > minimumPrice){
                    newProduct.addAssociatedPart(part);
                }
            }
            if(newProduct.getAssociatedParts().size() == 0) {
                throw new Exception("Produsul nu respecta cerinta");
            }
            return newProduct;
        }
    }

}
