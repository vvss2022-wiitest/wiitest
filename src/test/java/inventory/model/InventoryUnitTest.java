package inventory.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryUnitTest {
    Inventory inventory = null;

    final String partName = "DummyPart";
    final String productName = "DummyProduct";
    Part dummyPart = new InhousePart(1,partName,2.0,4,1,5,1);
    Product dummyProduct = new Product(1,productName,4.0,2,1,4, FXCollections.observableArrayList(dummyPart));
    @BeforeAll
    void setUp(){
        inventory = new Inventory();
        inventory.addPart(dummyPart);
        inventory.addProduct(dummyProduct);
    }
    @AfterAll
    void tearDown(){
        inventory.removeProduct(dummyProduct);
        inventory.deletePart(dummyPart);
    }

    @Test
    void lookupProductSuccess() {
        Product searchedProduct = inventory.lookupProduct(productName);
        assertEquals(searchedProduct, dummyProduct);
    }

    @Test
    void lookupPartSuccess() {
        Part searchedPart= inventory.lookupPart(partName);
        assertEquals(searchedPart, dummyPart);
    }
}