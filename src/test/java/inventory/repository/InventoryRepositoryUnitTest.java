package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryRepositoryUnitTest {
    private Inventory inventory;
    private InventoryRepository repository;

    final String partName = "DummyPart";
    final String productName = "DummyProduct";
    Part dummyPart = new InhousePart(1,partName,2.0,4,1,5,1);
    Product dummyProduct = new Product(1,productName,4.0,2,1,4, FXCollections.observableArrayList(dummyPart));

    @BeforeAll
    void setUp() {
        inventory = mock(Inventory.class);
        repository = new InventoryRepository("data/testItemsUnitRepo.txt",inventory);

    }

    @AfterAll
    void tearDown() {

    }

    @AfterEach
    void tearDownEach(){
        if(repository.getAllProducts().size() != 0)
            repository.deleteProduct(dummyProduct);
        if(repository.getAllParts().size() != 0)
            repository.deletePart(dummyPart);
        clearInvocations(inventory);
    }


    @Test
    void addPartSuccess() throws Exception {
        Mockito.doNothing().when(inventory).addPart(dummyPart);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(dummyPart));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(inventory.lookupPart(partName)).thenReturn(dummyPart);

        repository.addPart(dummyPart);

        Mockito.verify(inventory,times(1)).addPart(dummyPart);
        Mockito.verify(inventory,times(1)).getAllParts();
        Mockito.verify(inventory,times(1)).getProducts();

        assertEquals(1,repository.getAllParts().size());
        assertEquals(dummyPart,repository.lookupPart(partName));

        Mockito.verify(inventory,times(2)).getAllParts();
        Mockito.verify(inventory,times(1)).lookupPart(partName);
    }

    @Test
    void addProductSuccess() {
        Mockito.doNothing().when(inventory).addProduct(dummyProduct);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(dummyPart));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList(dummyProduct));
        Mockito.when(inventory.lookupProduct(productName)).thenReturn(dummyProduct);

        repository.addProduct(dummyProduct);

        Mockito.verify(inventory,times(1)).addProduct(dummyProduct);
        Mockito.verify(inventory,times(1)).getAllParts();
        Mockito.verify(inventory,times(1)).getProducts();

        assertEquals(1,repository.getAllProducts().size());
        assertEquals(dummyProduct,repository.lookupProduct(productName));

        Mockito.verify(inventory,times(2)).getProducts();
        Mockito.verify(inventory,times(1)).lookupProduct(productName);
    }
}