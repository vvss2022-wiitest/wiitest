package inventory.repository;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

/*
Used annotations:
@TestInstance(TestInstance.Lifecycle.PER_CLASS) - execute all test methods on the same test instance. (Helps make before all method not static)
@DisplayName("GivenName") - displays GivenName instead of method name when running the test
@Disabled - Ignores the test
@ParameterizedTest - run a test multiple times with different arguments
@ValueSource(strings = { "piesa1", "piesa2", "piesa3" }) - provides access to an array of literal values. Works with ParametrizedTest
@EmptySource  - provides access to an empty for supported types. Works with ParametrizedTest

* */


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryRepositoryTest {

    private InventoryRepository repository;
    private Part part;
    double maxPrice = 999999999.0;

    @BeforeAll
    void init(){
        repository = new InventoryRepository("data/testItems.txt");
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        if(repository.getAllParts().size() != 0)
            repository.deletePart(part);
    }

//    @DisplayName("ECP - Name VALID")
//    @ParameterizedTest
//    @ValueSource(strings = { "piesa1", "piesa2", "piesa3" })
    @Test
    void ecpNameValid() {
        part = new OutsourcedPart(0,"piesa1",1.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

//    @DisplayName("ECP - Name INVALID")
//    @ParameterizedTest
//    @EmptySource
    @Test
    void ecpNameInvalid(){
        part = new OutsourcedPart(0,"",1.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert false;
        } catch (Exception e) {
            assert (repository.getAllParts().size() == 0);
            assert (e.getMessage().equals("A name has not been entered."));
        }
    }

    @Test
    @DisplayName("ECP - Price VALID")
  //  @Disabled
    void ecpPriceValid(){
        part = new OutsourcedPart(0,"NumePiesa2",100.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    @DisplayName("ECP - Price INVALID")
    void ecpPriceInvalid(){
        part = new OutsourcedPart(0,"NumePiesa",-2.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert false;
        } catch (Exception e) {
            assert (repository.getAllParts().size() == 0);
            assert (e.getMessage().equals("The price must be greater than 0."));
        }
    }

    @Test
    void bvaTestNameLengthMax(){
        String nameMaxLen = "Q0BEX3GbV6O2HAaeAoSSZMwzELQjywzKXTZHHP27zRnpGgjXEKZ4y5bgAZk8hns8jftNkfln5MXBI2yNgkkY2Xz8N6PjG5OqlL7Eoq1AE5SSwZhlLyuHePhxb9Wbhq8hwZTGa4Prm25jYFWLETmjF2LAb0lKBWTRxvaOcIW2Ez20GXuiTAgwQn61BQFUKhcTVCvLF4G3GNyBKvHNbJdI4ab90kxzWHFam308eadGVRcZG9x7I6qoUKHLoSxTxQu";
        part = new OutsourcedPart(0,nameMaxLen,10.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    void bvaTestNameLengthAlmostMax(){
        String nameAlmostMaxLen = "0BEX3GbV6O2HAaeAoSSZMwzELQjywzKXTZHHP27zRnpGgjXEKZ4y5bgAZk8hns8jftNkfln5MXBI2yNgkkY2Xz8N6PjG5OqlL7Eoq1AE5SSwZhlLyuHePhxb9Wbhq8hwZTGa4Prm25jYFWLETmjF2LAb0lKBWTRxvaOcIW2Ez20GXuiTAgwQn61BQFUKhcTVCvLF4G3GNyBKvHNbJdI4ab90kxzWHFam308eadGVRcZG9x7I6qoUKHLoSxTxQu";
        part = new OutsourcedPart(0,nameAlmostMaxLen,10.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    void bvaTestNameOverLengthMax(){
        String nameMaxLen = "QQ0BEX3GbV6O2HAaeAoSSZMwzELQjywzKXTZHHP27zRnpGgjXEKZ4y5bgAZk8hns8jftNkfln5MXBI2yNgkkY2Xz8N6PjG5OqlL7Eoq1AE5SSwZhlLyuHePhxb9Wbhq8hwZTGa4Prm25jYFWLETmjF2LAb0lKBWTRxvaOcIW2Ez20GXuiTAgwQn61BQFUKhcTVCvLF4G3GNyBKvHNbJdI4ab90kxzWHFam308eadGVRcZG9x7I6qoUKHLoSxTxQu";
        part = new OutsourcedPart(0,nameMaxLen,10.0,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert false;
        } catch (Exception e) {
            assert (repository.getAllParts().size() == 0);
            assert (e.getMessage().equals("Name is too long!"));
        }
    }

    @Test
    void bvaTestMaxPrice(){
        part = new OutsourcedPart(0,"p",maxPrice,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    void bvaTestUnderMaxPrice(){
        part = new OutsourcedPart(0,"p",maxPrice-0.1,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert (repository.getAllParts().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    void bvaTestOverMaxPrice(){
        part = new OutsourcedPart(0,"p",maxPrice+0.1,5,1,10,"DummyCompany");
        try{
            assert (repository.getAllParts().size() == 0);
            repository.addPart(part);
            assert false;
        } catch (Exception e) {
            assert (repository.getAllParts().size() == 0);
            assert (e.getMessage().equals("The price is too high!"));
        }
    }



}