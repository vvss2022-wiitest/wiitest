package inventory.integration;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import static net.bytebuddy.matcher.ElementMatchers.none;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceRepoModelIntegrationTest {
    InventoryRepository repository;
    InventoryService service;

    final String partName = "DummyPart";
    final String productName = "DummyProduct";
    Part dummyPart = new InhousePart(1,partName,2.0,4,1,5,1);
    Product dummyProduct = new Product(1,productName,4.0,2,1,4, FXCollections.observableArrayList(dummyPart));

    @BeforeAll
    void setUp() {
        repository = new InventoryRepository("data/testItemsAllIntegration.txt");
        service = new InventoryService(repository);
    }

    @AfterAll
    void tearDownAll(){
        if(service.getAllProducts().size() != 0)
            service.deleteProduct(dummyProduct);
        if(service.getAllParts().size() != 0)
            service.deletePart(dummyPart);
    }


    @AfterEach
    void tearDownEach(){
        if(service.getAllProducts().size() != 0)
            service.deleteProduct(dummyProduct);
        if(service.getAllParts().size() != 0)
            service.deletePart(dummyPart);
    }

    @Test
    void testAddInhousePartSuccess() throws Exception {
        service.addInhousePart(dummyPart.getName(),dummyPart.getPrice(),dummyPart.getInStock(),dummyPart.getMin(),dummyPart.getMax(),dummyPart.getPartId());

        assertEquals(1,service.getAllParts().size());
        assertEquals(dummyPart,service.lookupPart(partName));
    }

    @Test
    void addProductSuccess(){
        service.addProduct(dummyProduct.getName(),dummyProduct.getPrice(),dummyProduct.getInStock(),dummyProduct.getMin(),dummyProduct.getMax(),FXCollections.observableArrayList(dummyPart));
        assertEquals(1,service.getAllProducts().size());
        assertEquals(dummyProduct,service.lookupProduct(productName));
    }
}

