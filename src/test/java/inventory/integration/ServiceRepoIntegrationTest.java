package inventory.integration;
import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceRepoIntegrationTest {
    Inventory inventory;
    InventoryRepository repository;
    InventoryService service;

    final String partName = "DummyPart";
    final String productName = "DummyProduct";
    Part dummyPart = new InhousePart(0,partName,2.0,4,1,5,0);
    Product dummyProduct = new Product(0,productName,4.0,2,1,4, FXCollections.observableArrayList(dummyPart));

    @BeforeAll
    void setUp() {
        inventory = mock(Inventory.class);
        repository = new InventoryRepository("data/testItemsRepoIntegration.txt",inventory);
        service = new InventoryService(repository);

    }

    @AfterEach
    void tearDownEach(){
        if(service.getAllProducts().size() != 0)
            service.deleteProduct(dummyProduct);
        if(service.getAllParts().size() != 0)
            service.deletePart(dummyPart);
        clearInvocations(inventory);
    }

    @AfterAll
    void tearDownAll(){
        if(service.getAllProducts().size() != 0)
            service.deleteProduct(dummyProduct);
        if(service.getAllParts().size() != 0)
            service.deletePart(dummyPart);
        clearInvocations(inventory);
    }



    @Test
    void testAddInhousePartSuccess() throws Exception {
        Mockito.doNothing().when(inventory).addPart(dummyPart);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList(dummyPart));
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(inventory.lookupPart(partName)).thenReturn(dummyPart);

        service.addInhousePart(dummyPart.getName(),dummyPart.getPrice(),dummyPart.getInStock(),dummyPart.getMin(),dummyPart.getMax(),dummyPart.getPartId());

     //   Mockito.verify(inventory,times(1)).addPart(dummyPart);
        Mockito.verify(inventory,times(1)).getAllParts();
        Mockito.verify(inventory,times(1)).getProducts();



        assertEquals(1,service.getAllParts().size());
        assertEquals(dummyPart,service.lookupPart(partName));

        Mockito.verify(inventory,times(2)).getAllParts();
        Mockito.verify(inventory,times(1)).lookupPart(partName);
    }

    @Test
    void addProductSuccess(){
        Mockito.doNothing().when(inventory).addProduct(dummyProduct);
        Mockito.when(inventory.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(inventory.lookupProduct(productName)).thenReturn(dummyProduct);
        Mockito.when(inventory.getProducts()).thenReturn(FXCollections.observableArrayList(dummyProduct));

        service.addProduct(dummyProduct.getName(),dummyProduct.getPrice(),dummyProduct.getInStock(),dummyProduct.getMin(),dummyProduct.getMax(),FXCollections.observableArrayList(dummyPart));

        Mockito.verify(inventory,times(1)).getAllParts();
        Mockito.verify(inventory,times(1)).getProducts();

        assertEquals(1,service.getAllProducts().size());
        assertEquals(dummyProduct,service.lookupProduct(productName));

        Mockito.verify(inventory,times(2)).getProducts();
        Mockito.verify(inventory,times(1)).lookupProduct(productName);
    }
}
