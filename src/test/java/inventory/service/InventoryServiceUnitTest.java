package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import static net.bytebuddy.matcher.ElementMatchers.none;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryServiceUnitTest {
    InventoryRepository repository;
    InventoryService service;

    final String partName = "DummyPart";
    final String productName = "DummyProduct";
    Part dummyPart = new InhousePart(0,partName,2.0,4,1,5,0);
    Product dummyProduct = new Product(1,productName,4.0,2,1,4, FXCollections.observableArrayList(dummyPart));

    @BeforeAll
    void setUp() {
        repository = mock(InventoryRepository.class, withSettings().useConstructor("data/testItemsUnitService.txt"));
        service = new InventoryService(repository);

    }

    @AfterEach
    void tearDown() {
    }

    @AfterEach
    void tearDownEach(){
        if(service.getAllProducts().size() != 0)
            service.deleteProduct(dummyProduct);
        if(service.getAllParts().size() != 0)
            service.deletePart(dummyPart);
        clearInvocations(repository);
    }

    @Test
    void testAddInhousePartSuccess() throws Exception {
        Mockito.doNothing().when(repository).addPart(dummyPart);
        Mockito.when(repository.getAllParts()).thenReturn(FXCollections.observableArrayList(dummyPart));
        Mockito.when(repository.lookupPart(partName)).thenReturn(dummyPart);
        Mockito.when(repository.getAllProducts()).thenReturn(FXCollections.observableArrayList());

        service.addInhousePart(dummyPart.getName(),dummyPart.getPrice(),dummyPart.getInStock(),dummyPart.getMin(),dummyPart.getMax(),dummyPart.getPartId());

        assertEquals(1,service.getAllParts().size());
        assertEquals(dummyPart,service.lookupPart(partName));

        Mockito.verify(repository,times(1)).getAllParts();
        Mockito.verify(repository,times(1)).lookupPart(partName);
    }

    @Test
    void addProductSuccess(){
        Mockito.doNothing().when(repository).addProduct(dummyProduct);
        Mockito.when(repository.getAllParts()).thenReturn(FXCollections.observableArrayList());
        Mockito.when(repository.lookupProduct(productName)).thenReturn(dummyProduct);
        Mockito.when(repository.getAllProducts()).thenReturn(FXCollections.observableArrayList(dummyProduct));

        service.addProduct(dummyProduct.getName(),dummyProduct.getPrice(),dummyProduct.getInStock(),dummyProduct.getMin(),dummyProduct.getMax(),FXCollections.observableArrayList(dummyPart));

        assertEquals(1,service.getAllProducts().size());
        assertEquals(dummyProduct,service.lookupProduct(productName));

        Mockito.verify(repository,times(1)).getAllProducts();
        Mockito.verify(repository,times(1)).lookupProduct(productName);
    }
}