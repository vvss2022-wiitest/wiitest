package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InventoryServiceTest {

    private InventoryRepository repository;
    private InventoryService service;

    @BeforeAll
    void init(){
        repository = new InventoryRepository("data/items.txt");
        service = new InventoryService(repository);
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @DisplayName("TC01")
    @ParameterizedTest
    @ValueSource(ints = { 0, -1 })
    void inputsValid(int minimumPrice) {
        Exception ex = assertThrows(Exception.class,
                () -> service.getSpecialProduct("any", minimumPrice));
        assertTrue(ex.getMessage().contains("Date de intrare invalide"));
    }

    @DisplayName("TC02")
    @ParameterizedTest
    @ValueSource(strings = { "", "dsa" })
    void nameValid(String name) {
        Exception ex = assertThrows(Exception.class,
                () -> service.getSpecialProduct(name, 1));
        assertTrue(ex.getMessage().contains("Produsul nu a fost gasit"));
    }


    @DisplayName("TC03")
    @Test
    void testProductWithPartsThatDoNotRespectTheCondition() {
        Exception ex = assertThrows(Exception.class,
                () -> service.getSpecialProduct("products4", 5));
        assertTrue(ex.getMessage().contains("Produsul nu respecta cerinta"));
    }

    @DisplayName("TC04")
    @Test
    void testProductWithPartsThatRespectTheCondition() throws Exception {
        Product product = service.getSpecialProduct("Clock", 1);
        assertEquals(product.getAssociatedParts().size(), 2);
        assertEquals(product.getAssociatedParts().get(0).getName(), "Cog");
        assertEquals(product.getAssociatedParts().get(1).getName(), "Spring");
    }

    @DisplayName("TC05")
    @Test
    void testProductWithAllPartsThatRespectTheCondition() throws Exception {
        Product newProduct = service.getSpecialProduct("product2", 1);
        Product oldProduct = repository.lookupProduct("product2");
        assertEquals(oldProduct.toString(), newProduct.toString());
        assertEquals(oldProduct.getAssociatedParts().toString(), newProduct.getAssociatedParts().toString());
    }

}